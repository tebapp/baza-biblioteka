-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 20 Sty 2019, 13:39
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Baza danych: `iii_sem`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `autorzy`
--

CREATE TABLE `autorzy` (
  `id_autor` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `biografia` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategorie`
--

CREATE TABLE `kategorie` (
  `id_kategoria` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `opis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klienci`
--

CREATE TABLE `klienci` (
  `id_klient` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `data_przystapienia` int(11) NOT NULL,
  `data_skreslenia` int(11) DEFAULT NULL,
  `saldo` decimal(5,2) NOT NULL,
  `rabat` decimal(3,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kody_pocztowe`
--

CREATE TABLE `kody_pocztowe` (
  `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL,
  `nazwa` char(6) NOT NULL,
  `id_miejscowosc` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ksiazki`
--

CREATE TABLE `ksiazki` (
  `id_ksiazka` bigint(20) UNSIGNED NOT NULL,
  `tytul` varchar(100) NOT NULL,
  `id_autor` bigint(20) UNSIGNED NOT NULL,
  `id_wydawnictwo` bigint(20) UNSIGNED NOT NULL,
  `rok` int(11) NOT NULL,
  `numer_wydania` int(11) NOT NULL,
  `isbn` char(16) NOT NULL,
  `id_kategoria` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `miejscowosci`
--

CREATE TABLE `miejscowosci` (
  `id_miejscowosc` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `osoby`
--

CREATE TABLE `osoby` (
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `imie` varchar(30) NOT NULL,
  `nazwisko` varchar(35) NOT NULL,
  `pesel` char(11) NOT NULL,
  `id_ulica` bigint(20) UNSIGNED DEFAULT NULL,
  `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL,
  `nr_posesja` varchar(8) NOT NULL,
  `nr_lokal` varchar(8) NOT NULL,
  `telefon` char(12) NOT NULL,
  `e_mail` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownicy`
--

CREATE TABLE `pracownicy` (
  `id_pracownik` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `data_zatrudnienia` date NOT NULL,
  `data_zwolnienia` date DEFAULT NULL,
  `id_stanowisko` bigint(20) UNSIGNED NOT NULL,
  `mnoznik_pensji` decimal(3,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stanowiska`
--

CREATE TABLE `stanowiska` (
  `id_stanowisko` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(20) NOT NULL,
  `zakres` text NOT NULL,
  `pensja` decimal(7,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ulice`
--

CREATE TABLE `ulice` (
  `id_ulica` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wydawnictwa`
--

CREATE TABLE `wydawnictwa` (
  `id_wydawnictwo` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `rok_powstania` date NOT NULL,
  `charakterystyka` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wypozyczenia`
--

CREATE TABLE `wypozyczenia` (
  `id_wypozyczenie` bigint(20) UNSIGNED NOT NULL,
  `data_wypozyczenia` date NOT NULL,
  `dni_wypozyczenia` int(10) UNSIGNED NOT NULL,
  `wartosc_wypozyczenia` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wypozyczenia_klienci`
--

CREATE TABLE `wypozyczenia_klienci` (
  `id_wypozyczenie` bigint(20) UNSIGNED NOT NULL,
  `id_klient` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wypozyczenie_ksiazka`
--

CREATE TABLE `wypozyczenie_ksiazka` (
  `id_wypozyczenie` bigint(20) UNSIGNED NOT NULL,
  `id_ksiazka` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `autorzy`
--
ALTER TABLE `autorzy`
  ADD UNIQUE KEY `id_autor` (`id_autor`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- Indexes for table `kategorie`
--
ALTER TABLE `kategorie`
  ADD UNIQUE KEY `id_kategoria` (`id_kategoria`);

--
-- Indexes for table `klienci`
--
ALTER TABLE `klienci`
  ADD UNIQUE KEY `id_klient` (`id_klient`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- Indexes for table `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  ADD UNIQUE KEY `id_kod_pocztowy` (`id_kod_pocztowy`),
  ADD KEY `id_miejscowosc` (`id_miejscowosc`);

--
-- Indexes for table `ksiazki`
--
ALTER TABLE `ksiazki`
  ADD UNIQUE KEY `id_ksiazka` (`id_ksiazka`),
  ADD KEY `id_kategoria` (`id_kategoria`),
  ADD KEY `id_autor` (`id_autor`),
  ADD KEY `id_wydawnictwo` (`id_wydawnictwo`);

--
-- Indexes for table `miejscowosci`
--
ALTER TABLE `miejscowosci`
  ADD UNIQUE KEY `id_miejscowosc` (`id_miejscowosc`);

--
-- Indexes for table `osoby`
--
ALTER TABLE `osoby`
  ADD UNIQUE KEY `id_osoba` (`id_osoba`),
  ADD KEY `id_ulica` (`id_ulica`),
  ADD KEY `id_kod_pocztowy` (`id_kod_pocztowy`);

--
-- Indexes for table `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD UNIQUE KEY `id_pracownik` (`id_pracownik`),
  ADD KEY `id_osoba` (`id_osoba`),
  ADD KEY `id_stanowisko` (`id_stanowisko`);

--
-- Indexes for table `stanowiska`
--
ALTER TABLE `stanowiska`
  ADD UNIQUE KEY `id_stanowisko` (`id_stanowisko`);

--
-- Indexes for table `ulice`
--
ALTER TABLE `ulice`
  ADD UNIQUE KEY `id_ulica` (`id_ulica`);

--
-- Indexes for table `wydawnictwa`
--
ALTER TABLE `wydawnictwa`
  ADD UNIQUE KEY `id_wydawnictwo` (`id_wydawnictwo`);

--
-- Indexes for table `wypozyczenia`
--
ALTER TABLE `wypozyczenia`
  ADD UNIQUE KEY `id_wypozyczenie` (`id_wypozyczenie`);

--
-- Indexes for table `wypozyczenia_klienci`
--
ALTER TABLE `wypozyczenia_klienci`
  ADD KEY `id_wypozyczenie` (`id_wypozyczenie`),
  ADD KEY `id_klient` (`id_klient`);

--
-- Indexes for table `wypozyczenie_ksiazka`
--
ALTER TABLE `wypozyczenie_ksiazka`
  ADD KEY `id_wypozyczenie` (`id_wypozyczenie`),
  ADD KEY `id_ksiazka` (`id_ksiazka`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `autorzy`
--
ALTER TABLE `autorzy`
  MODIFY `id_autor` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `kategorie`
--
ALTER TABLE `kategorie`
  MODIFY `id_kategoria` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `klienci`
--
ALTER TABLE `klienci`
  MODIFY `id_klient` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  MODIFY `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ksiazki`
--
ALTER TABLE `ksiazki`
  MODIFY `id_ksiazka` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `miejscowosci`
--
ALTER TABLE `miejscowosci`
  MODIFY `id_miejscowosc` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `osoby`
--
ALTER TABLE `osoby`
  MODIFY `id_osoba` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  MODIFY `id_pracownik` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `stanowiska`
--
ALTER TABLE `stanowiska`
  MODIFY `id_stanowisko` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ulice`
--
ALTER TABLE `ulice`
  MODIFY `id_ulica` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `wydawnictwa`
--
ALTER TABLE `wydawnictwa`
  MODIFY `id_wydawnictwo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `wypozyczenia`
--
ALTER TABLE `wypozyczenia`
  MODIFY `id_wypozyczenie` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `autorzy`
--
ALTER TABLE `autorzy`
  ADD CONSTRAINT `autorzy_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `klienci`
--
ALTER TABLE `klienci`
  ADD CONSTRAINT `klienci_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  ADD CONSTRAINT `kody_pocztowe_ibfk_1` FOREIGN KEY (`id_miejscowosc`) REFERENCES `miejscowosci` (`id_miejscowosc`);

--
-- Ograniczenia dla tabeli `ksiazki`
--
ALTER TABLE `ksiazki`
  ADD CONSTRAINT `ksiazki_ibfk_1` FOREIGN KEY (`id_kategoria`) REFERENCES `kategorie` (`id_kategoria`),
  ADD CONSTRAINT `ksiazki_ibfk_2` FOREIGN KEY (`id_kategoria`) REFERENCES `kategorie` (`id_kategoria`),
  ADD CONSTRAINT `ksiazki_ibfk_3` FOREIGN KEY (`id_kategoria`) REFERENCES `kategorie` (`id_kategoria`),
  ADD CONSTRAINT `ksiazki_ibfk_4` FOREIGN KEY (`id_kategoria`) REFERENCES `kategorie` (`id_kategoria`),
  ADD CONSTRAINT `ksiazki_ibfk_5` FOREIGN KEY (`id_autor`) REFERENCES `autorzy` (`id_autor`),
  ADD CONSTRAINT `ksiazki_ibfk_6` FOREIGN KEY (`id_wydawnictwo`) REFERENCES `wydawnictwa` (`id_wydawnictwo`);

--
-- Ograniczenia dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD CONSTRAINT `osoby_ibfk_1` FOREIGN KEY (`id_ulica`) REFERENCES `ulice` (`id_ulica`),
  ADD CONSTRAINT `osoby_ibfk_2` FOREIGN KEY (`id_kod_pocztowy`) REFERENCES `kody_pocztowe` (`id_kod_pocztowy`);

--
-- Ograniczenia dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD CONSTRAINT `pracownicy_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`),
  ADD CONSTRAINT `pracownicy_ibfk_2` FOREIGN KEY (`id_stanowisko`) REFERENCES `stanowiska` (`id_stanowisko`);

--
-- Ograniczenia dla tabeli `wypozyczenia_klienci`
--
ALTER TABLE `wypozyczenia_klienci`
  ADD CONSTRAINT `wypozyczenia_klienci_ibfk_1` FOREIGN KEY (`id_klient`) REFERENCES `klienci` (`id_klient`),
  ADD CONSTRAINT `wypozyczenia_klienci_ibfk_2` FOREIGN KEY (`id_wypozyczenie`) REFERENCES `wypozyczenia` (`id_wypozyczenie`);

--
-- Ograniczenia dla tabeli `wypozyczenie_ksiazka`
--
ALTER TABLE `wypozyczenie_ksiazka`
  ADD CONSTRAINT `wypozyczenie_ksiazka_ibfk_1` FOREIGN KEY (`id_ksiazka`) REFERENCES `ksiazki` (`id_ksiazka`),
  ADD CONSTRAINT `wypozyczenie_ksiazka_ibfk_2` FOREIGN KEY (`id_wypozyczenie`) REFERENCES `wypozyczenia` (`id_wypozyczenie`);
COMMIT;
